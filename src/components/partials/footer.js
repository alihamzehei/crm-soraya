const Footer = () => {
    return (
        <>
            <footer id="page-footer" className="opacity-0" style={{ opacity: 1 }}>
                <div className="content py-20 font-size-xs clearfix">
                    <div className="float-right">
                        Crafted with <i className="fa fa-heart text-pulse" /> by <a className="font-w600" href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
                    </div>
                    <div className="float-left">
                        <a className="font-w600" href="https://goo.gl/po9Usv" target="_blank">Codebase 2.0</a> © <span className="js-year-copy">2017-21</span>
                    </div>
                </div>
            </footer>

        </>
    )
}
export default Footer;